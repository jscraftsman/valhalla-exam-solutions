function solutionOne (input) {
	return input.split(' ').reverse().join(' ')
}

function solutionTwo(input) {
	let highest = input[0]
	let lowest = input[0]

	for (let current of input) {
		if (current >= highest) {
			highest = current 
		}
		if (current <= lowest) {
			lowest = current 
		}
	}

	return {
		highest,
		lowest
	}
}

function solutionThree(input) {
	let sortedArray = input.slice()
	let arraySize = sortedArray.length

	for (let i = arraySize; i >= 0; i--) {
		for (let j = 0; j <= i; j++) {
			if (sortedArray[j - 1] > sortedArray[j]) {
				let temp = sortedArray[j - 1]
				sortedArray[j - 1] = sortedArray[j]
				sortedArray[j] = temp
			}
		}
	}

	return sortedArray
}

function solutionFour(input) {
	let characters = input.split('')
	let recurringCharacter = null
	let recurringSet = {}

	for (let character of characters) {
		if (recurringSet[character]) {
			recurringCharacter = character
			break
		} else {
			recurringSet[character] = character
		}
	}

	return recurringCharacter
}

function solutionFive(input) {
	if (input.length === 1 && input[0] === 8) return 'Yes'

	let sum = 0

	for (let i in input) {
		for (let j in input) {
			if (i === j) continue

			if ((input[i] + input[j])  === 8) {
				return 'Yes'
			}
		}
	}

	return 'No' 
}
