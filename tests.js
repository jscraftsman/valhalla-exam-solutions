QUnit.module('Question 1')
QUnit.test('One word', (assert) => {
	const input = 'Valhalla'
	const expected = 'Valhalla'
	const actual = solutionOne(input)

	assert.equal(expected, actual)
});

QUnit.test('Two words', (assert) => {
	const input = 'Valhalla Awesome'
	const expected = 'Awesome Valhalla'
	const actual = solutionOne(input)

	assert.equal(expected, actual)
});

QUnit.test('Three words', (assert) => {
	const input = 'Valhalla is Awesome'
	const expected = 'Awesome is Valhalla'
	const actual = solutionOne(input)

	assert.equal(expected, actual)
});

QUnit.module('Question 2')
QUnit.test('One item array', (assert) => {
	const input = [1] 
	const expected = {
		highest: 1,
		lowest: 1
	}
	const actual = solutionTwo(input)

	assert.deepEqual(expected, actual)
});

QUnit.test('Two item array', (assert) => {
	const testSet = [
		{ input: [2, 1], expected: {highest: 2, lowest: 1} },
		{ input: [1, 2], expected: {highest: 2, lowest: 1} }
	]

	for (let test of testSet) {
		let actual = solutionTwo(test.input)
		assert.deepEqual(test.expected, actual)
	}
});

QUnit.test('Three item array', (assert) => {
	const testSet = [
		{ input: [0, 1, 2], expected: {highest: 2, lowest: 0} },
		{ input: [0, -1, 1], expected: {highest: 1, lowest: -1} },
	]

	for (let test of testSet) {
		let actual = solutionTwo(test.input)
		assert.deepEqual(test.expected, actual)
	}
});

QUnit.test('Random number of items array', (assert) => {
	const testSet = [
		{ input: [34, 7, 23, 32, 5, 62, -1], expected: {highest: 62, lowest: -1} },
		{ input: [100, -9999, 34, 7, 23, 32, 5, 62, -1], expected: {highest: 100, lowest: -9999} },
		{ input: [-100, 100, 777, -9999, 34, 7, 500, 23, 32, 5, 62, -1], expected: {highest: 777, lowest: -9999} }
	]

	for (let test of testSet) {
		let actual = solutionTwo(test.input)
		assert.deepEqual(test.expected, actual)
	}
});

QUnit.module('Question 3')
QUnit.test('One item array', (assert) => {
	const input = [1]
	const expected = [1]
	const actual = solutionThree(input)

	assert.deepEqual(expected, actual)
});

QUnit.test('Two items array', (assert) => {
	const input = [2, 1]
	const expected = [1, 2]
	const actual = solutionThree(input)

	assert.deepEqual(expected, actual)
});

QUnit.test('Three items array', (assert) => {
	const input = [2, 3, 1]
	const expected = [1, 2, 3]
	const actual = solutionThree(input)

	assert.deepEqual(expected, actual)
});

QUnit.test('Array with negative items', (assert) => {
	const input = [1, -1, 0]
	const expected = [-1, 0, 1]
	const actual = solutionThree(input)

	assert.deepEqual(expected, actual)
});

QUnit.test('Random number of items array', (assert) => {
	const input = [34, 7, 23, 32, 5, 62]
	const expected = [5, 7, 23, 32, 34, 62] 
	const actual = solutionThree(input)

	console.table(expected)
	console.table(actual)
	assert.deepEqual(expected, actual)
});

QUnit.module('Question 4')
QUnit.test('One character string', (assert) => {
	const input = 'A' 
	const expected = null 
	const actual = solutionFour(input)

	assert.equal(expected, actual)
});

QUnit.test('Two character string without recurring character', (assert) => {
	const input = 'AB' 
	const expected = null 
	const actual = solutionFour(input)

	assert.equal(expected, actual)
});

QUnit.test('Two character string with recurring character', (assert) => {
	const input = 'AA' 
	const expected = 'A' 
	const actual = solutionFour(input)

	assert.equal(expected, actual)
});

QUnit.test('Three character string without recurring character', (assert) => {
	const input = 'ABC' 
	const expected = null 
	const actual = solutionFour(input)

	assert.equal(expected, actual)
});

QUnit.test('Three character string with recurring character', (assert) => {
	const input = 'ABA' 
	const expected = 'A' 
	const actual = solutionFour(input)

	assert.equal(expected, actual)
});

QUnit.test('Has multiple recurring characters', (assert) => {
	const input = 'CABDBA' 
	const expected = 'B' 
	const actual = solutionFour(input)

	assert.equal(expected, actual)
});

QUnit.module('Question 5')
QUnit.test('One item array with sum not equal to 8', (assert) => {
	const input = [4]
	const expected = 'No'
	const actual = solutionFive(input)

	assert.equal(expected, actual)
});

QUnit.test('One item array with sum equal to 8', (assert) => {
	const input = [8]
	const expected = 'Yes'
	const actual = solutionFive(input)

	assert.equal(expected, actual)
});

QUnit.test('Two item array with two numbers with a sum not equal to 8', (assert) => {
	const input = [1, 2]
	const expected = 'No'
	const actual = solutionFive(input)

	assert.equal(expected, actual)
});

QUnit.test('Two item array with two numbers with a sum equal to 8', (assert) => {
	const input = [4, 4]
	const expected = 'Yes'
	const actual = solutionFive(input)

	assert.equal(expected, actual)
});

QUnit.test('Three item array with two numbers with a sum not equal to 8', (assert) => {
	const testSet = [
		{ input: [1, 2, 3], expected: 'No' },
		{ input: [2, 2, 3], expected: 'No' },
		{ input: [2, 2, 2], expected: 'No' },
	]

	for (let test of testSet) {
		let actual = solutionFive(test.input)
		assert.equal(test.expected, actual)
	}
});

QUnit.test('Three item array with two numbers with a sum equal to 8', (assert) => {
	const testSet = [
		{ input: [4, 4, 3], expected: 'Yes' },
		{ input: [1, 7, 3], expected: 'Yes' },
		{ input: [2, 6, 3], expected: 'Yes' },
	]

	for (let test of testSet) {
		let actual = solutionFive(test.input)
		assert.equal(test.expected, actual)
	}
});

QUnit.test('Four item array with two numbers with a sum not equal to 8', (assert) => {
	const testSet = [
		{ input: [1, 2, 3, 4], expected: 'No' },
		{ input: [4, 3, 2, 1], expected: 'No' },
		{ input: [4, 3, 2, 1], expected: 'No' },
	]

	for (let test of testSet) {
		let actual = solutionFive(test.input)
		assert.equal(test.expected, actual)
	}
});

QUnit.test('Four item array with two numbers with a sum equal to 8', (assert) => {
	const testSet = [
		{ input: [4, 2, 4, 1], expected: 'Yes' },
		{ input: [7, 2, 4, 1], expected: 'Yes' },
		{ input: [7, 8, 0, 1], expected: 'Yes' },
	]

	for (let test of testSet) {
		let actual = solutionFive(test.input)
		assert.equal(test.expected, actual)
	}
});

QUnit.test('Random number of item array with two numbers with a sum equal to 8', (assert) => {
	const testSet = [
		{ input: [7, 2, 4, 6, 1], expected: 'Yes' },
		{ input: [8, 2, 4, 5, 1, 3, 0], expected: 'Yes' },
		{ input: [5, 1, 4, 7, 2, 3, 2], expected: 'Yes' },
	]

	for (let test of testSet) {
		let actual = solutionFive(test.input)
		assert.equal(test.expected, actual)
	}
});
